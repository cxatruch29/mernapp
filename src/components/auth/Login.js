import React, { useState } from 'react';


const Login=()=>{

    //state iniciar sesion
    const [usuario,guardarUsuario]=useState({
        email:'',
        password:''
        });
    
//extraer usuario
const {email,password}=usuario;


const onChange=e=>{
    guardarUsuario({
        ...usuario,[e.target.name]:e.target.value
    })
}
// SUBMIT

const onSubmit=(e)=>{

    e.preventdefault();
}



    return (

            <div className="form-usuario">

                <div className="contenedor-form sombra-dark">

                    <h1>
                        Iniciar Sesion
                    </h1>

                    <form
                        onSubmit={onSubmit}

                    
                    />



                    <form>

                        <div className="campo-form">

                            <label htmlFor="email">
                                Email
                            </label>

                            <input
                            
                            type="email"
                            id="email"
                            name="email"
                            placeholder="tu email"
                            onChange={onChange}
                            value={email}
                            />

                        </div>

                         <div className="campo-form">

                            <label htmlFor="password">
                                Password
                            </label>

                            <input
                            
                            type="password"
                            id="password"
                            name="password"
                            placeholder="tu password"
                            onChange={onChange}
                            value={password}

                            />

                        </div>


                        <div className="campo-form">
                            <input type="submit" className="btn btn-primario btn-block"
                            value="Iniciar Sesion"  
                            />

                        </div>





                    </form>


                </div>


            </div>

    );
}

export default Login;