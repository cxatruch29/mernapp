import React from 'react';
import Proyecto from './Proyecto';
const ListadoProyectos=()=>{

    const proyectos=[
        {nombre:'Tienda'},
        {nombre:'Intranet'},
        {nombre:'DiseÑo de sitio web'}
    
    ]

    
    return (

        <ul className="listado-proyectos">
           {proyectos.map(proyecto=>(
                
                <Proyecto
                    proyecto={proyecto}
                />
                
            ))}

        

        </ul>

    );

}

export default ListadoProyectos;