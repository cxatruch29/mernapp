import React, { Fragment, useState } from 'react';

const NuevoProyecto=()=>{


const [proyecto,guardarProyecto]=useState({

    nombre:''

});


// extraer nombre

const onChangeProyecto=(e)=>{
    guardarProyecto({
            ...proyecto, [e.target.name]:e.target.value

    })

}
    
    return (
        <Fragment>

            
        
        <button type="button"
        className="bn btn-block btn-primario"
        
        > Nuevo Proyecto 


        </button>
        <form className="formulario-nuevo-proyecto">

            <input 
                 type="text"
                 className="input-text"
                 placeholder="Nombre proyecto"
                 name="nombre"
                 onChange={onChangeProyecto}
                 />


            <input
                type="submit"
                className="btn btn-primario btn-block"
                value="Agregar proyecto"/>


        </form>

        </Fragment>

    );
    

}

export default NuevoProyecto;